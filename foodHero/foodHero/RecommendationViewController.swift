//
//  RecommendationViewController.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import UIKit
import CoreData

class RecommendationViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var RecommandCollectionView: UICollectionView!
    var recipeList = [NSManagedObject]()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // RecommandCollectionView
        RecommandCollectionView.delegate = self
        RecommandCollectionView.dataSource = self
        
        RecommandCollectionView.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
        RecommandCollectionView.collectionViewLayout = layout 
        RecommandCollectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadRecipes()
    }
    
    func loadRecipes() {
        DatabaseStorage.sharedInstance.loadAllRecipesFromRest(appDelegate)
        recipeList = DatabaseStorage.sharedInstance.loadAllRecipesFromCoreData(appDelegate)
        RecommandCollectionView.reloadData()
    }

    //MARK: recommandCollectionView delegates
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipeList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = RecommandCollectionView.dequeueReusableCellWithReuseIdentifier(RecCellIdentifier, forIndexPath: indexPath) as! RecommandCollectionViewCell
        let rec = recipeList[indexPath.row]
        cell.titleLbael.text = rec.valueForKey("name") as? String
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 150, height: 150);
    }
}
