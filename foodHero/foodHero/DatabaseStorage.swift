//
//  DatabaseStorage.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import Foundation
import CoreData

class DatabaseStorage {
    let persistenceStack = PersistenceStack()
    
    class var sharedInstance: DatabaseStorage {
        struct Static{
            static var instance: DatabaseStorage?
            static var token: dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = DatabaseStorage()
        }
        return Static.instance!
    }
    
    /**
    Loads all recipes from the database
    */
    func loadAllRecipesFromCoreData(appDelegate: AppDelegate) -> [NSManagedObject] {
        let managedContext = appDelegate.managedObjectContext
        //fetch from Core Data all recipe entries
        let fetchRequest = NSFetchRequest(entityName: "Recipe")
        //hand fetchRequest to managed object context
        do{
            if let results = try managedContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                return results
            }
        } catch let err as NSError {
            print("[LikeViewController] Error: Could not fetch data: " + err.localizedDescription)
        }
        return []
    }
    
    /**
    Loads all recipes from the Rest API and stores them in the coreData
    */
    func loadAllRecipesFromRest(appDelegate: AppDelegate) {
        //get a managed object context
        let url = NSURL(string: apiPrefix)
        let urlRequest = NSURLRequest(URL: url!)
        NSURLConnection.sendAsynchronousRequest(urlRequest, queue: NSOperationQueue.mainQueue()) { (response , data, error) -> Void in
            if error != nil && data != nil {
                print(error)
                return
            }
            do {
                let testString  = "[ { \"about\" : \"this is a test recipe 1\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Pasta\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, }, { \"about\" : \"this is a test recipe 2\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Pizza\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, }, { \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Apple Pie\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, }, { \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Apple Pie\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, },{ \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Pancakes\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, },{ \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Crêpes\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, },{ \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Rosted Chicken\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, },{ \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Stuffed Peppers\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, },{ \"about\" : \"this is a test recipe 3\", \"calories\" : 50.0, \"carbs\" : 584, \"categories\" : \"fast food\", \"cholesterol\" : 20.0, \"fat\" : 3.5, \"proteine\" : 5.6, \"sodium\" : 45, \"cooktime\" : 60, \"identifier\" : \"recipe\", \"ingredients\" : \"apples, oranges\", \"instructions\"  : \"step 1, step 2, step 3\", \"name\" : \"Lasagne\", \"numRatings\" : 5, \"preptime\" : 15, \"rating\" : 4.8, \"totalTime\" : 75, } ]"
                if let json = try NSJSONSerialization.JSONObjectWithData(testString.dataUsingEncoding(4)!, options: NSJSONReadingOptions.MutableContainers) as? [[String : AnyObject]] {
                    
                    for var recipeJson : [String : AnyObject] in json {
                        let about = recipeJson[kRecipieAboutKey] as! String
                        let calories = recipeJson[kRecipieCaloriesKey] as! Float
                        let carbs = recipeJson[kRecipieCarbsKey] as! Float
                        let categories = recipeJson[kRecipieCategoriesKey] as! String
                        let cholesterol = recipeJson[kRecipieCholesterolKey] as! Float
                        let cooktime = recipeJson[kRecipieCooktimeKey] as! NSInteger
                        let fat = recipeJson[kRecipieFatKey] as! Float
                        let identifier = recipeJson[kRecipieIdentifierKey] as! String
                        let ingredients = recipeJson[kRecipieIngredientsKey] as! String
                        let instructions = recipeJson[kRecipieInstructionsKey] as! String
                        let name = recipeJson[kRecipieNameKey] as! String
                        let numRatings = recipeJson[kRecipieNumRatingsKey] as! NSInteger
                        let preptime = recipeJson[kRecipiePreptimeKey] as! NSInteger
                        let proteine = recipeJson[kRecipieProteineKey] as! Float
                        let rating = recipeJson[kRecipieRatingKey] as! Float
                        let sodium = recipeJson[kRecipieSodiumKey] as! Float
                        let totalTime = recipeJson[kRecipieTotalTimeKey] as! NSInteger
                        
                        Recipe.createRecipe(about, calories: calories, carbs: carbs, categories: categories, cholesterol: cholesterol, cooktime: integer_t(cooktime), fat: fat, identifier: identifier, ingredients: ingredients, instructions: instructions, name: name, numRatings: integer_t(numRatings), preptime: integer_t(preptime), proteine: proteine, rating: rating, sodium: sodium, totalTime: integer_t(totalTime), context: appDelegate.managedObjectContext)
                    }
                }
            }catch let err as NSError {
                print("Error:" + err.localizedDescription)
            }
        }
    }
    
    /**
    Deletes a recipes from the database
    */
    func deleteRecipe(recipe: Recipe) {
        persistenceStack.managedObjectContext.deleteObject(recipe)
        persistenceStack.saveContext()
    }
    
    /**
    Saves a recipe to database
    */
    func saveNameForRecipe(withName: String, appDelegate: AppDelegate) {
        let managedContext = appDelegate.managedObjectContext
        //create new managed object and insert it into managedContext
        let entity = NSEntityDescription.entityForName("Recipe", inManagedObjectContext: managedContext)
        let rec = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        //set name attribute using KVC
        rec.setValue(withName, forKey: "name")
        //commit changes and save to disk
        do{
            try managedContext.save()
        } catch let err as NSError {
            print("[LikeViewController] Error: Could not save data:" + err.localizedDescription)
        }
    }
    
    /**
    Saves the context of the persistence stack
    */
    func saveContext() {
        persistenceStack.saveContext()
    }
}
