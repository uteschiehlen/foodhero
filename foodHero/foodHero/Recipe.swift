//
//  Recipe.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import Foundation
import CoreData

@objc(Recipe)
class Recipe: ModelObject {
    override class var entityName: String {
        return "Recipe"
    }
    
    @NSManaged var about: String
    @NSManaged var calories: Float
    @NSManaged var carbs: Float
    @NSManaged var categories: String
    @NSManaged var cholesterol: Float
    @NSManaged var cooktime: integer_t
    @NSManaged var fat: Float
    @NSManaged var identifier: String
    @NSManaged var ingredients: String
    @NSManaged var instructions: String
    @NSManaged var name: String
    @NSManaged var numRatings: integer_t
    @NSManaged var preptime: integer_t
    @NSManaged var proteine: Float
    @NSManaged var rating: Float
    @NSManaged var sodium: Float
    @NSManaged var totalTime: integer_t
    
    class func createRecipe(about: String, calories: Float, carbs: Float, categories: String,
        cholesterol: Float, cooktime: integer_t, fat: Float, identifier: String, ingredients: String,
        instructions: String, name: String, numRatings: integer_t, preptime: integer_t, proteine: Float,
        rating: Float, sodium: Float, totalTime: integer_t, context: NSManagedObjectContext) -> Recipe {
            let recipe = self.insertNewObject(context) as! Recipe
        
            recipe.about = about
            recipe.calories = calories
            recipe.carbs = carbs
            recipe.categories = categories
            recipe.cholesterol = cholesterol
            recipe.cooktime = cooktime
            recipe.fat = fat
            recipe.identifier = identifier
            recipe.ingredients = ingredients
            recipe.instructions = instructions
            recipe.name = name
            recipe.numRatings = numRatings
            recipe.preptime = preptime
            recipe.proteine = proteine
            recipe.rating = rating
            recipe.sodium = sodium
            recipe.totalTime = totalTime
        
            return recipe
    }

}