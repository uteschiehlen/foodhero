//
//  ModelObject.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import UIKit
import CoreData

class ModelObject: NSManagedObject {
    class var entityName: String {
        return ""
    }
    
    class func insertNewObject(context: NSManagedObjectContext) -> NSManagedObject {
        return NSEntityDescription.insertNewObjectForEntityForName(entityName,
            inManagedObjectContext: context)
    }
}
