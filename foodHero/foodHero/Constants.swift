//
//  Constants.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//
import Foundation
import UIKit

//settingsViewController
let textCellIdentifier = "textCell"
let switchCellIdentifier = "switchCell"
let textFieldCellIdentifier = "textFieldCell"
let cellNames = ["Name", "Preferences", "Diet" , "Popup", "Credits"]
let userName = "Ute Emily"

let collectionCell = ["Pizza", "Sushi", "Soup", "Dumplings"]
let RecCellIdentifier = "recCell"

let screenSize: CGRect = UIScreen.mainScreen().bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height


//RestfulService
let apiPrefix = "https://itunes.apple.com/us/rss/topgrossingipadapplications/limit=25/json"

//json keys
let kRecipieAboutKey = "about"
let kRecipieCaloriesKey = "calories"
let kRecipieCarbsKey = "carbs"
let kRecipieCategoriesKey = "categories"
let kRecipieCholesterolKey = "cholesterol"
let kRecipieFatKey = "fat"
let kRecipieProteineKey = "proteine"
let kRecipieSodiumKey = "sodium"
let kRecipieCooktimeKey = "cooktime"
let kRecipieIdentifierKey = "identifier"
let kRecipieIngredientsKey = "ingredients"
let kRecipieInstructionsKey = "instructions"
let kRecipieNameKey = "name"
let kRecipieNumRatingsKey = "numRatings"
let kRecipiePreptimeKey = "preptime"
let kRecipieRatingKey = "rating"
let kRecipieTotalTimeKey = "totalTime"

//array containing the names of the recipes which are liked
var likeRecipes = [String]()