//
//  SettingViewController.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var settingTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //settingsTableView
        settingTableView.delegate = self
        settingTableView.dataSource = self
        settingTableView.rowHeight = UITableViewAutomaticDimension
        settingTableView.estimatedRowHeight = 100.0
        
        settingTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: settingTableView delegates
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellNames.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            return createTextFieldCellAtImagePath(indexPath)
        }
        else if indexPath.row == 2 || indexPath.row == 3
        {
            return createSwitchCellAtImagePath(indexPath)
        }
        else
        {
            return createTextCellAtImagePath(indexPath)
        }
    }
    
    func createTextCellAtImagePath(indexPath:NSIndexPath) -> UITableViewCell {
        let cell = self.settingTableView.dequeueReusableCellWithIdentifier(textCellIdentifier,
            forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = cellNames[indexPath.row]
        return cell
    }
    
    func createSwitchCellAtImagePath(indexPath:NSIndexPath) -> SwitchCell {
        let cell = self.settingTableView.dequeueReusableCellWithIdentifier(switchCellIdentifier) as! SwitchCell
        cell.titleLabel.text  = cellNames[indexPath.row]
        cell.onOffSwitch.on = false
        cell.onOffSwitch.addTarget(self, action: "switchValueDidChange", forControlEvents: .ValueChanged)
        return cell
    }
    
    func createTextFieldCellAtImagePath(indexPath:NSIndexPath) -> TextFieldCell {
        let cell = self.settingTableView.dequeueReusableCellWithIdentifier(textFieldCellIdentifier,
        forIndexPath: indexPath) as! TextFieldCell
        cell.titleLabel.text = cellNames[indexPath.row]
        cell.nameField.text = userName
        cell.nameField.addTarget(self, action: "nameDidChange", forControlEvents: .EditingDidEnd)
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1 {
            performSegueWithIdentifier("preference", sender: self)
        }
    }
    
    func switchValueDidChange(){
        print("Switch changed")
    }
    
    func nameDidChange(){
        print("Name changed")
    }
}



