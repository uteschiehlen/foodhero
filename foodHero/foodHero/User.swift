//
//  User.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import Foundation
import CoreData

class User: ModelObject {
    override class var entityName: String {
        return "User"
    }
    
    @NSManaged var identifier: String
    @NSManaged var username: String
    
    class func createUser(identifier: String, username: String, context: NSManagedObjectContext) -> User {
        var user = self.insertNewObject(context) as! User
        
        user.identifier = identifier
        user.username = username
        
        return user
    }
}