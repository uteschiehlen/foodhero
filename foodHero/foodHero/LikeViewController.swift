//
//  LikeViewController.swift
//  foodHero
//
//  Created by Ute Schiehlen on 19.09.15.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import UIKit
import CoreData

class LikeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let likeList = UITableView(frame: CGRect(x: 0, y: 100, width: 380, height: 500))
    var recipeList = [NSManagedObject]()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        likeList.delegate = self
        likeList.dataSource = self
        likeList.registerClass(SwitchCell.self, forCellReuseIdentifier: switchCellIdentifier)
        view.addSubview(likeList)
        likeList.reloadData()
    }

    /*
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadRecipes()
    }
    */
    
    // MARK: settingTableView delegates
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return recipeList.count
        return likeRecipes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return createTextCellAtImagePath(indexPath)
    }
    
    func createTextCellAtImagePath(indexPath:NSIndexPath) -> UITableViewCell {
        let cell = self.likeList.dequeueReusableCellWithIdentifier(switchCellIdentifier,
            forIndexPath: indexPath) as! SwitchCell
        //let rec = recipeList[indexPath.row]
        //cell.textLabel?.text = rec.valueForKey("name") as? String
        cell.textLabel?.text = likeRecipes[indexPath.row]
        return cell
    }
    
    /*
    func loadRecipes(){
        recipeList = DatabaseStorage.sharedInstance.loadAllRecipesFromCoreData(appDelegate)
        likeList.reloadData()
    }
    */
}
