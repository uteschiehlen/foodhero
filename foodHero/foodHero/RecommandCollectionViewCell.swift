//
//  RecommandCollectionViewCell.swift
//  foodHero
//
//  Created by Emily Huang on 2015-09-19.
//  Copyright © 2015 Ute Schiehlen. All rights reserved.
//

import UIKit

class RecommandCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet var titleLbael: UILabel!
    @IBOutlet var heart: UIButton!
    @IBOutlet var dish: UIImageView!
    
    
    @IBAction func buttonPressed(sender: AnyObject) {
        print("Button pressed")
        if heart.imageView?.image == UIImage(named: "unheart")
        {
            heart.setImage(UIImage(named:"heart"), forState: .Normal)
            // TODO: add to liked later
            likeRecipes.append(titleLbael.text!)
        }
        else {
            heart.setImage(UIImage(named:"unheart"), forState: .Normal)
            // TODO: do removed from liked later
        }
    }
}